import numpy as np
from random import *
import plotly.graph_objects as go
import pandas as pd


def random_sample_binary(M=20, N=8):
    return np.array([np.random.randint(2,size=N) for x in range(M)])

def penalty(candidate, N, objs, capacity):
    r = []
    w = 0
    price = 0
    for i in range(N):
        w = w + (objs[i]['w']*candidate[i])
        r.append(objs[i]["v"]/objs[i]["w"])
        price = price + (objs[i]['w']*candidate[i])
    rho = max(r)
    if w <= capacity:
        return 0
    return (price - capacity)*rho

def fitness(candidates, N, objs, capacity):
    fits = []
    for candidate in candidates:
        fit = 0
        for i in range(N):
            fit = fit + (objs[i]['v'] * candidate[i])
        fit = fit - penalty(candidate, N, objs, capacity)
        fits.append(fit)
    return fits

def roulette(fits, M):
    roll = np.random.random()
    total_fit = sum(fits)
    bottom = 0
    for i in range(M):
        top = bottom + (fits[i]/total_fit)
        if roll <= top:
            match = i
            break
        else:
            bottom = top
    return match

def crossover(parent1, parent2, crossover_prob):
    if random() < crossover_prob:
        n = len(parent1)
        offsprings = np.zeros((2, n))
        pos = np.random.permutation(range(n+1))[0]
        offsprings[0][0:pos] = parent1[0:pos]
        offsprings[1][0:pos] = parent2[0:pos]
        offsprings[0][pos:n+1] = parent2[pos:n+1]
        offsprings[1][pos:n+1] = parent1[pos:n+1]
        return offsprings
    return np.array([parent1, parent2])

def mutate(candidate, mutation_prob):
    if random() < mutation_prob:
        n = len(candidate)
        candidate[np.random.permutation(range(n))[0]] = 0 if candidate[np.random.permutation(range(n))[0]] else 1
    return candidate

capacity = 35
objs = {
    0: {"w": 10, "v": 5},
    1: {"w": 18, "v": 8},
    2: {"w": 12, "v": 7},
    3: {"w": 14, "v": 6},
    4: {"w": 13, "v": 9},
    5: {"w": 11, "v": 5},
    6: {"w": 8, "v": 4},
    7: {"w": 6, "v": 3},
}
N = len(objs)
M = 20
crossover_prob = 0.65
mutation_prob = 0.12
k = 1

# Random starting solutions
candidates = random_sample_binary(M, N)

# Calculate Fitness
fits = fitness(candidates, N, objs, capacity)
fits_mean = [np.mean(fits)]
fits_best = [max(fits)]
fits_history = pd.DataFrame(columns=['fit', 'gen'])
for i in fits:
    fits_history = fits_history.append({'fit': i, 'gen': k-1}, ignore_index=True)

for i in range(1000):
    new_candidates = []
    # Roulette
    for i in range(int(M/2)):
        roll = [roulette(fits, M), roulette(fits, M)]
        son = crossover(candidates[roll[0]], candidates[roll[1]], crossover_prob)
        son[0] = mutate(son[0], mutation_prob)
        son[1] = mutate(son[1], mutation_prob)
        new_candidates.append(son[0])
        new_candidates.append(son[1])
    candidates = np.array(new_candidates)
    k = k+1
    fits = fitness(candidates, N, objs, capacity)
    fits_best.append(max(fits))
    fits_mean.append(np.mean(fits))
    for i in fits:
        fits_history = fits_history.append({'fit': i, 'gen': k-1}, ignore_index=True)
    if max(fits) == 21:
        break

fits = fitness(candidates, N, objs, capacity)
solution = candidates[np.argmax(fits)]
print("Solution: " + str(solution))

# Creating plot
fig = go.Figure()
fig.add_trace(go.Scatter(y=fits_history['fit'], x=fits_history['gen'],
                    mode='markers',
                    name='Fitness',
                    opacity=0.6))
fig.add_trace(go.Scatter(y=fits_best,
                    mode='lines+markers',
                    name='Best Fitness'))
fig.add_trace(go.Scatter(y=fits_mean,
                    mode='lines+markers',
                    name='Mean Fitness'))
fig.update_layout(title='N-Queens',
                   xaxis_title='Generation',
                   yaxis_title='Fitness')
fig.show()
